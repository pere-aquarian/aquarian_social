# Copyright 2021 Pere Martínez <pere@aquarian.tech>

{
    "name": "Mail Template Body Only",
    "version": "13.0.1.1.0",
    "category": "Social",
    "author": "Aquarian,"
    "Odoo Community Association (OCA)",
    "website": "https://github.com/OCA/sale-workflow",
    "license": "AGPL-3",
    "depends": ["mail"],
    "data": [
        "reports/mail_notification_paynow.xml",
    ],
    "installable": True,
}
